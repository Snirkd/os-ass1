// A simple program that just prints stuff on screen

#include "types.h"
#include "stat.h"
#include "user.h"

int
main(void)
{
	printf(1, "My first user program on xv6\n");
	printf(1, "Printing stuff.. done printing the stuff\n");
	printf(1, "No supporting system call created for it!!!\n");
	printf(1, "And now, there is going to be some CPS printout\n");

	cps();

	exit();
}