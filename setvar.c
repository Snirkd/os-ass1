#include "types.h" 
#include "stat.h" 
#include "user.h" 
#include "fcntl.h" 

// mandatory for the new system call so it can run on a new "forked" process. 
// this file is the application itself.
int
main(int argc, char *argv[])
{
	char* varName;
	char* value;

	if(argc < 3){
    	printf(2, "Too few arguments for setvar...\n");
    	exit();
  	}

  	varName = argv[1];
  	value = argv[2];

	
	setvar(varName , value);

	exit();
}