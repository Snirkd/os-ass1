// Shell.

#include "types.h"
#include "user.h"
#include "fcntl.h"

// Parsed command representation
#define EXEC  1
#define REDIR 2
#define PIPE  3
#define LIST  4
#define BACK  5

#define MAXARGS 10

#define HISTORY_RECORD_MAX_LENGTH 128
#define HISTORY_MAX_RECORDS 16
#define HISTORY_ACTUAL_LENGTH (HISTORY_MAX_RECORDS + 1)
#define PLUS_N_MOD_HISTORY_ACTUAL_LENGTH(X, n) (X + n)%HISTORY_ACTUAL_LENGTH

struct cmd {
  int type;
};

struct execcmd {
  int type;
  char *argv[MAXARGS];
  char *eargv[MAXARGS];
};

struct redircmd {
  int type;
  struct cmd *cmd;
  char *file;
  char *efile;
  int mode;
  int fd;
};

struct pipecmd {
  int type;
  struct cmd *left;
  struct cmd *right;
};

struct listcmd {
  int type;
  struct cmd *left;
  struct cmd *right;
};

struct backcmd {
  int type;
  struct cmd *cmd;
};

int fork1(void);  // Fork but panics on failure.
void panic(char*);
struct cmd *parsecmd(char*);

// Execute cmd.  Never returns.
void
runcmd(struct cmd *cmd)
{
  int p[2];
  struct backcmd *bcmd;
  struct execcmd *ecmd;
  struct listcmd *lcmd;
  struct pipecmd *pcmd;
  struct redircmd *rcmd;

  if(cmd == 0)
    exit();

  switch(cmd->type){
  default:
    panic("runcmd");

  case EXEC:
    ecmd = (struct execcmd*)cmd;
    if(ecmd->argv[0] == 0)
      exit();
    exec(ecmd->argv[0], ecmd->argv); // prints the exec: fail line
    printf(2, "exec %s failed\n", ecmd->argv[0]);
    break;

  case REDIR:
    rcmd = (struct redircmd*)cmd;
    close(rcmd->fd);
    if(open(rcmd->file, rcmd->mode) < 0){
      printf(2, "open %s failed\n", rcmd->file);
      exit();
    }
    runcmd(rcmd->cmd);
    break;

  case LIST:
    lcmd = (struct listcmd*)cmd;
    if(fork1() == 0)
      runcmd(lcmd->left);
    wait();
    runcmd(lcmd->right);
    break;

  case PIPE:
    pcmd = (struct pipecmd*)cmd;
    if(pipe(p) < 0)
      panic("pipe");
    if(fork1() == 0){
      close(1);
      dup(p[1]);
      close(p[0]);
      close(p[1]);
      runcmd(pcmd->left);
    }
    if(fork1() == 0){
      close(0);
      dup(p[0]);
      close(p[0]);
      close(p[1]);
      runcmd(pcmd->right);
    }
    close(p[0]);
    close(p[1]);
    wait();
    wait();
    break;

  case BACK:
    bcmd = (struct backcmd*)cmd;
    if(fork1() == 0)
      runcmd(bcmd->cmd);
    break;
  }
  exit();
}

int
getcmd(char *buf, int nbuf)
{
  printf(2, "$ ");
  memset(buf, 0, nbuf);
  gets(buf, nbuf);
  if(buf[0] == 0) // EOF
    return -1;
  return 0;
}

void //AVI
inserttohistory(char* buf, char* history[], uint* most_recent, uint* least_recent)
{
  if((*least_recent-*most_recent+HISTORY_ACTUAL_LENGTH)%HISTORY_ACTUAL_LENGTH==1) {
        // If the stack is full, remove the first element entered there
        free(history[*least_recent]);
        *least_recent = PLUS_N_MOD_HISTORY_ACTUAL_LENGTH(*least_recent, 1);
      }
      // insert the command in the buffer to the history as the most recent command.
      history[*most_recent] = malloc(strlen(buf));
      memset(history[*most_recent], 0, strlen(buf));
      buf[strlen(buf)-1] = 0;  // chop \n
      strcpy(history[*most_recent], buf);
      buf[strlen(buf)] = '\n';  // unchop \n
      // advance most recent command pointer
      *most_recent = PLUS_N_MOD_HISTORY_ACTUAL_LENGTH(*most_recent, 1);
}

int //AVI
replaceglobalvars(char* buf)
{
  // DEBUG prints
  // printf(2, "[DEBUG] initial buf is: %s\n", buf);
  // printf(2, "[DEBUG] initial strlen(buf): %d\n", strlen(buf));

  // local variables initialization
  char *buf_varstart; // points to the start of the first variable (the $ sign)
  char *buf_varend; // points to the end of the first variable (it could point to $ of ' ' or '\n')
  char buf_suffix[128]; // buffer to hold the suffix of the command (from thebuf_varend and so on)
  char var[32]; // the extracted variable name
  char val[128]; // holder for the variable value

  // advance buf_varstart to the next variable
  buf_varstart = strpbrk(buf, "$");

  while(buf_varstart != 0) {
    // initialize buf_varend to point to the first letter of the variable
    buf_varend = buf_varstart+1;

    // loop over the variable and get its name into var
    int i = 0;
    while(*buf_varend != '$' && *buf_varend != ' ' && *buf_varend != '\n'){
      var[i] = *buf_varend;
      i++;
      buf_varend++;
    }

    // reset buf_suffix and copy into it the suffix of buf
    memset(buf_suffix, 0, sizeof(buf_suffix));
    strcpy(buf_suffix, buf_varend);

    // reset val and get the value of var into it
    int retval = 0;
    memset(val, 0, sizeof(val));
    retval = getvar(var, val);
    // strcpy(val, "Mekudeshet");

    // check the return value of getvar. if it is -1, exit the function and return -1.
    if(retval == -1) {
      return -1;
    }

    // DEBUG prints
    // printf(2, "[DEBUG] ==============================\n");
    // printf(2, "[DEBUG] buf: %s\n", buf);
    // printf(2, "[DEBUG] strlen(buf): %d\n", strlen(buf));
    // printf(2, "[DEBUG] buf_varstart: %s\n", buf_varstart);
    // printf(2, "[DEBUG] strlen(buf_varstart): %d\n", strlen(buf_varstart));
    // printf(2, "[DEBUG] buf_varend: %s\n", buf_varend);
    // printf(2, "[DEBUG] strlen(buf_varend): %d\n", strlen(buf_varend));
    // printf(2, "[DEBUG] buf_suffix: %s\n", buf_suffix);
    // printf(2, "[DEBUG] sizeof(buf_suffix): %d\n", sizeof(buf_suffix));
    // printf(2, "[DEBUG] var: %s\n", var);
    // printf(2, "[DEBUG] sizeof(var): %d\n", sizeof(var));
    // printf(2, "[DEBUG] val: %s\n", val);
    // printf(2, "[DEBUG] sizeof(val): %d\n", sizeof(val));
    // printf(2, "[DEBUG] ==============================\n");

    // replace the variable with the value
    memset(buf_varstart, 0, strlen(buf_varstart));
    strcpy(buf_varstart, val);

    // advance buf_varstart to the end of the copied value
    buf_varstart = buf_varstart + strlen(val);

    // concatenate the suffix
    strcpy(buf_varstart, buf_suffix);

    // DEBUG prints
    // printf(2, "[DEBUG] ++++++++++++++++++++++++++++++\n");
    // printf(2, "[DEBUG] buf: %s\n", buf);
    // printf(2, "[DEBUG] ++++++++++++++++++++++++++++++\n");
    
    // buf_varstart++;
    buf_varstart = strpbrk(buf, "$");
  }

  // DEBUG prints
  // printf(2, "[DEBUG] replaced buf is: %s\n", buf);
  // printf(2, "[DEBUG] replaced strlen(buf): %d\n", strlen(buf));

  return 0;
}

void //AVI
inserttoglobalvartable(char* buf)
{
  // DEBUG prints
  // printf(2, "[DEBUG] initial buf is: %s\n", buf);
  // printf(2, "[DEBUG] initial strlen(buf): %d\n", strlen(buf));

  // local variables initialization
  char *buf_valstart;
  char *buf_varstart;
  char val[128];
  char var[32];

  // chop the '\n'
  buf[strlen(buf)-1] = 0;

  // copy the variable value to val
  buf_valstart = strpbrk(buf, "=");
  buf_valstart++;
  memset(val, 0, sizeof(val));
  strcpy(val, buf_valstart);

  // DEBUG print
  // printf(2, "[DEBUG] val: %s\n", val);

  // chop the '=' and all of the following buf
  buf_valstart--;
  memset(buf_valstart, 0, strlen(buf_valstart));

  // copy the variable name to var
  buf_varstart = buf;
  memset(var, 0, sizeof(var));
  strcpy(var, buf_varstart);

  // DEBUG print
  // printf(2, "[DEBUG] var: %s\n", var);

  // setting the variables
  int setvarres = setvar(var, val);

  if(setvarres == -1){
    printf(2, "No room for additional variables\n");
  } else if (setvarres == -2){
    printf(2, "Input is illegal\n");
  }
}

int
main(void)
{
  static char buf[128];
  static char temp_buf[128]; // temporary buffer for copy purposes
  int fd;

  // Ensure that three file descriptors are open.
  while((fd = open("console", O_RDWR)) >= 0){
    if(fd >= 3){
      close(fd);
      break;
    }
  }

  // Initialize the history save mechanism.
  char *history[HISTORY_ACTUAL_LENGTH];
  uint most_recent = 0;
  uint least_recent = 0;

  // Read the first command.
  int getcmd_result = getcmd(buf, sizeof(buf));

  // Read and run input commands.
  while(getcmd_result >= 0){
    // ==================================================================================================================================================
    // Insert the current command as the most recent command into the history. meaning that the the most recent command inserted will be at place say, 5.
    // ========================================== Do this only if the command is not a "history -l XX" command. =========================================
    // ==================================================================================================================================================
    if(!(buf[0] == 'h' && buf[1] == 'i' && buf[2] == 's' && buf[3] == 't' && buf[4] == 'o' && buf[5] == 'r' && buf[6] == 'y' && buf[7] == ' ' && buf[8] == '-' && buf[9] == 'l' && buf[10] == ' ')) {
      // saving every command that is not "history -l XX" to the history
      inserttohistory(buf, history, &most_recent, &least_recent);
    }

    // ==================================================================================================================================================
    // ================================================= substitute any global variables in the command. ================================================
    // ==================================================================================================================================================
    int replace_res = replaceglobalvars(buf);
    if(replace_res == -1) {
      // throwing error message
      printf(2, "Attempt to use unset variables\n");

      // getting the next command
      getcmd_result = getcmd(buf, sizeof(buf)); // get the next command
      continue;
    }


    // ==================================================================================================================================================
    // ============================================================== handling the command. =============================================================
    // ==================================================================================================================================================
    if(strchr(buf, '=') != 0){
      inserttoglobalvartable(buf);

      // getting the next command
      getcmd_result = getcmd(buf, sizeof(buf)); // get the next command
      continue;
    }else if(buf[0] == 'h' && buf[1] == 'i' && buf[2] == 's' && buf[3] == 't' && buf[4] == 'o' && buf[5] == 'r' && buf[6] == 'y' && buf[7] == ' ' && buf[8] == '-' && buf[9] == 'l' && buf[10] == ' '){
      // History -l must be called by the parent, not the child.
      if(strlen(buf) < 13) {
        // throwing error message
        printf(2, "No index supplied for history command\n");

        // saving the "history -l XX" command to the history
        if(replace_res != 0){
          inserttohistory(buf, history, &most_recent, &least_recent);
        }

        // getting the next command
        getcmd_result = getcmd(buf, sizeof(buf)); // get the next command
        continue;
      }
      else if((strlen(buf) == 13 && (buf[11] < 48 || buf[11] > 57)) ||
              (strlen(buf) == 13 && (buf[11] >= 48 && buf[11] <= 57) && buf[11]-'0' > (most_recent-least_recent+HISTORY_ACTUAL_LENGTH)%HISTORY_ACTUAL_LENGTH ) ||
              (strlen(buf) == 14 && ((buf[11] < 48 || buf[11] > 57) || (buf[12] < 48 || buf[12] > 57))) ||
              (strlen(buf) == 14 && ((buf[11] >= 48 && buf[11] <= 57) && (buf[12] >= 48 && buf[12] <= 57)) && ((buf[11]-'0')*10 + (buf[12]-'0')) > (most_recent-least_recent+HISTORY_ACTUAL_LENGTH)%HISTORY_ACTUAL_LENGTH) ||
              strlen(buf) > 14) {
        // throwing error message
        printf(2, "Bad index. Expected an integer within bounds of %d - %d\n", 1, (most_recent-least_recent+HISTORY_ACTUAL_LENGTH)%HISTORY_ACTUAL_LENGTH);

        // saving the "history -l XX" command to the history
        if(replace_res != 0){
          inserttohistory(buf, history, &most_recent, &least_recent);
        }

        // getting the next command
        getcmd_result = getcmd(buf, sizeof(buf)); // get the next command
        continue;
      }
      else {
        // save the command "history -l XX" in a temp value.
        memset(temp_buf, 0, sizeof(buf));
        strcpy(temp_buf, buf);

        // manually set the buff to the command at the history.
        if(strlen(buf) == 13) {
          // extract the history record number from user input
          int ind = buf[11]-'0';

          // translate the history record number to the actual array number
          ind = PLUS_N_MOD_HISTORY_ACTUAL_LENGTH(least_recent-1+HISTORY_ACTUAL_LENGTH, ind);
          
          // set the buff to the command at the history in place ind
          memset(buf, 0, sizeof(buf));
          strcpy(buf, history[ind]);
          buf[strlen(buf)] = '\n';  // unchop \n
        }
        else {
          // extract the history record number from user input
          int ind = (buf[11]-'0')*10 + (buf[12]-'0');
          
          // translate the history record number to the actual array number
          ind = PLUS_N_MOD_HISTORY_ACTUAL_LENGTH(least_recent-1+HISTORY_ACTUAL_LENGTH, ind);
          
          // set the buff to the command at the history in place ind
          memset(buf, 0, sizeof(buf));
          strcpy(buf, history[ind]);
          buf[strlen(buf)] = '\n';  // unchop \n
        }

        // saving the "history -l XX" command to the history
        if(replace_res != 0){
          inserttohistory(temp_buf, history, &most_recent, &least_recent);
        }

        // replace again the variables in the command that came back from history.
        replace_res = replaceglobalvars(buf);
        if(replace_res == -1) {
          // throwing error message
          printf(2, "Attempt to use unset variables\n");

          // getting the next command
          getcmd_result = getcmd(buf, sizeof(buf)); // get the next command
          continue;
        }
      }
    }
    if(buf[0] == 'h' && buf[1] == 'i' && buf[2] == 's' && buf[3] == 't' && buf[4] == 'o' && buf[5] == 'r' && buf[6] == 'y' && buf[7] == '\n'){
      // History must be called by the parent, not the child.
      buf[strlen(buf)-1] = 0;  // chop \n
      int i = least_recent;
      int j = 1;
      while(i!=most_recent) {
        printf(2, "%d %s\n", j, history[i]);
        i = PLUS_N_MOD_HISTORY_ACTUAL_LENGTH(i, 1);
        j++;
      }
      getcmd_result = getcmd(buf, sizeof(buf)); // get the next command
      continue;
    }
    if(buf[0] == 'c' && buf[1] == 'd' && buf[2] == ' '){
      // chdir must be called by the parent, not the child.
      buf[strlen(buf)-1] = 0;  // chop \n
      if(chdir(buf+3) < 0)
        printf(2, "cannot cd %s\n", buf+3);
      getcmd_result = getcmd(buf, sizeof(buf)); // get the next command
      continue;
    }

    int chpid = fork1();

   	if(chpid == 0){
      runcmd(parsecmd(buf));
    }

    wait();

    // int wtime = 0; //AVI - debug printouts for wait2
    // int rtime = 0; //AVI - debug printouts for wait2
    // int iotime = 0; //AVI - debug printouts for wait2
    // wait2(chpid, &wtime, &rtime, &iotime); //AVI - debug printouts for wait2
    // printf(2, "child PID: %d, child wtime: %d, child iotime: %d, child rtime: %d\n", chpid, wtime, iotime, rtime); //AVI - debug printouts for wait2
    getcmd_result = getcmd(buf, sizeof(buf)); // get the next command
  }
  exit();
}

void
panic(char *s)
{
  printf(2, "%s\n", s);
  exit();
}

int
fork1(void)
{
  int pid;

  pid = fork();
  if(pid == -1)
    panic("fork");
  return pid;
}

//PAGEBREAK!
// Constructors

struct cmd*
execcmd(void)
{
  struct execcmd *cmd;

  cmd = malloc(sizeof(*cmd));
  memset(cmd, 0, sizeof(*cmd));
  cmd->type = EXEC;
  return (struct cmd*)cmd;
}

struct cmd*
redircmd(struct cmd *subcmd, char *file, char *efile, int mode, int fd)
{
  struct redircmd *cmd;

  cmd = malloc(sizeof(*cmd));
  memset(cmd, 0, sizeof(*cmd));
  cmd->type = REDIR;
  cmd->cmd = subcmd;
  cmd->file = file;
  cmd->efile = efile;
  cmd->mode = mode;
  cmd->fd = fd;
  return (struct cmd*)cmd;
}

struct cmd*
pipecmd(struct cmd *left, struct cmd *right)
{
  struct pipecmd *cmd;

  cmd = malloc(sizeof(*cmd));
  memset(cmd, 0, sizeof(*cmd));
  cmd->type = PIPE;
  cmd->left = left;
  cmd->right = right;
  return (struct cmd*)cmd;
}

struct cmd*
listcmd(struct cmd *left, struct cmd *right)
{
  struct listcmd *cmd;

  cmd = malloc(sizeof(*cmd));
  memset(cmd, 0, sizeof(*cmd));
  cmd->type = LIST;
  cmd->left = left;
  cmd->right = right;
  return (struct cmd*)cmd;
}

struct cmd*
backcmd(struct cmd *subcmd)
{
  struct backcmd *cmd;

  cmd = malloc(sizeof(*cmd));
  memset(cmd, 0, sizeof(*cmd));
  cmd->type = BACK;
  cmd->cmd = subcmd;
  return (struct cmd*)cmd;
}
//PAGEBREAK!
// Parsing

char whitespace[] = " \t\r\n\v";
char symbols[] = "<|>&;()";

int
gettoken(char **ps, char *es, char **q, char **eq)
{
  char *s;
  int ret;

  s = *ps;
  while(s < es && strchr(whitespace, *s))
    s++;
  if(q)
    *q = s;
  ret = *s;
  switch(*s){
  case 0:
    break;
  case '|':
  case '(':
  case ')':
  case ';':
  case '&':
  case '<':
    s++;
    break;
  case '>':
    s++;
    if(*s == '>'){
      ret = '+';
      s++;
    }
    break;
  default:
    ret = 'a';
    while(s < es && !strchr(whitespace, *s) && !strchr(symbols, *s))
      s++;
    break;
  }
  if(eq)
    *eq = s;

  while(s < es && strchr(whitespace, *s))
    s++;
  *ps = s;
  return ret;
}

int
peek(char **ps, char *es, char *toks)
{
  char *s;

  s = *ps;
  while(s < es && strchr(whitespace, *s))
    s++;
  *ps = s;
  return *s && strchr(toks, *s);
}

struct cmd *parseline(char**, char*);
struct cmd *parsepipe(char**, char*);
struct cmd *parseexec(char**, char*);
struct cmd *nulterminate(struct cmd*);

struct cmd*
parsecmd(char *s)
{
  char *es;
  struct cmd *cmd;

  es = s + strlen(s); // TO REMOVE: pointer calculation - s + it's length is the position it's null terminator \0.
  cmd = parseline(&s, es);
  peek(&s, es, "");
  if(s != es){
    printf(2, "leftovers: %s\n", s);
    panic("syntax");
  }
  nulterminate(cmd);
  return cmd;
}

struct cmd*
parseline(char **ps, char *es)
{
  struct cmd *cmd;

  cmd = parsepipe(ps, es);
  while(peek(ps, es, "&")){
    gettoken(ps, es, 0, 0);
    cmd = backcmd(cmd);
  }
  if(peek(ps, es, ";")){
    gettoken(ps, es, 0, 0);
    cmd = listcmd(cmd, parseline(ps, es));
  }
  return cmd;
}

struct cmd*
parsepipe(char **ps, char *es)
{
  struct cmd *cmd;

  cmd = parseexec(ps, es);
  if(peek(ps, es, "|")){
    gettoken(ps, es, 0, 0);
    cmd = pipecmd(cmd, parsepipe(ps, es));
  }
  return cmd;
}

struct cmd*
parseredirs(struct cmd *cmd, char **ps, char *es)
{
  int tok;
  char *q, *eq;

  while(peek(ps, es, "<>")){
    tok = gettoken(ps, es, 0, 0);
    if(gettoken(ps, es, &q, &eq) != 'a')
      panic("missing file for redirection");
    switch(tok){
    case '<':
      cmd = redircmd(cmd, q, eq, O_RDONLY, 0);
      break;
    case '>':
      cmd = redircmd(cmd, q, eq, O_WRONLY|O_CREATE, 1);
      break;
    case '+':  // >>
      cmd = redircmd(cmd, q, eq, O_WRONLY|O_CREATE, 1);
      break;
    }
  }
  return cmd;
}

struct cmd*
parseblock(char **ps, char *es)
{
  struct cmd *cmd;

  if(!peek(ps, es, "("))
    panic("parseblock");
  gettoken(ps, es, 0, 0);
  cmd = parseline(ps, es);
  if(!peek(ps, es, ")"))
    panic("syntax - missing )");
  gettoken(ps, es, 0, 0);
  cmd = parseredirs(cmd, ps, es);
  return cmd;
}

struct cmd*
parseexec(char **ps, char *es)
{
  char *q, *eq;
  int tok, argc;
  struct execcmd *cmd;
  struct cmd *ret;

  if(peek(ps, es, "("))
    return parseblock(ps, es);

  ret = execcmd();
  cmd = (struct execcmd*)ret;

  argc = 0;
  ret = parseredirs(ret, ps, es);
  while(!peek(ps, es, "|)&;")){
    if((tok=gettoken(ps, es, &q, &eq)) == 0)
      break;
    if(tok != 'a')
      panic("syntax");
    cmd->argv[argc] = q;
    cmd->eargv[argc] = eq;
    argc++;
    if(argc >= MAXARGS)
      panic("too many args");
    ret = parseredirs(ret, ps, es);
  }
  cmd->argv[argc] = 0;
  cmd->eargv[argc] = 0;
  return ret;
}

// NUL-terminate all the counted strings.
struct cmd*
nulterminate(struct cmd *cmd)
{
  int i;
  struct backcmd *bcmd;
  struct execcmd *ecmd;
  struct listcmd *lcmd;
  struct pipecmd *pcmd;
  struct redircmd *rcmd;

  if(cmd == 0)
    return 0;

  switch(cmd->type){
  case EXEC:
    ecmd = (struct execcmd*)cmd;
    for(i=0; ecmd->argv[i]; i++)
      *ecmd->eargv[i] = 0;
    break;

  case REDIR:
    rcmd = (struct redircmd*)cmd;
    nulterminate(rcmd->cmd);
    *rcmd->efile = 0;
    break;

  case PIPE:
    pcmd = (struct pipecmd*)cmd;
    nulterminate(pcmd->left);
    nulterminate(pcmd->right);
    break;

  case LIST:
    lcmd = (struct listcmd*)cmd;
    nulterminate(lcmd->left);
    nulterminate(lcmd->right);
    break;

  case BACK:
    bcmd = (struct backcmd*)cmd;
    nulterminate(bcmd->cmd);
    break;
  }
  return cmd;
}
