#include "types.h"
#include "x86.h"
#include "defs.h"
#include "date.h"
#include "param.h"
#include "memlayout.h"
#include "mmu.h"
#include "proc.h"


int sys_yield(void)
{
  yield(); 
  return 0;
}

int
sys_fork(void)
{
  return fork();
}

int
sys_exit(void)
{
  exit();
  return 0;  // not reached
}

int
sys_wait(void)
{
  return wait();
}

int
sys_kill(void)
{
  int pid;

  if(argint(0, &pid) < 0)
    return -1;
  return kill(pid);
}

int
sys_getpid(void)
{
  return myproc()->pid;
}

int
sys_sbrk(void)
{
  int addr;
  int n;

  if(argint(0, &n) < 0)
    return -1;
  addr = myproc()->sz;
  if(growproc(n) < 0)
    return -1;
  return addr;
}

int
sys_sleep(void)
{
  int n;
  uint ticks0;

  if(argint(0, &n) < 0)
    return -1;
  acquire(&tickslock);
  ticks0 = ticks;
  while(ticks - ticks0 < n){
    if(myproc()->killed){
      release(&tickslock);
      return -1;
    }
    sleep(&ticks, &tickslock);
  }
  release(&tickslock);
  return 0;
}

// return how many clock tick interrupts have occurred
// since start.
int
sys_uptime(void)
{
  uint xticks;

  acquire(&tickslock);
  xticks = ticks;
  release(&tickslock);
  return xticks;
}

// Prints out information about the processes
int
sys_cps(void)
{
  return cps();
}

/* SNIR: Note: to get the arguments for a function, 
         all functions here must get void and then call the arguments like this:
         say we have setVariable(char* variable)
         we want to access the variable like this:
              
              char* variable;
              argstr(0 , &variable); // argument at place 0, first arg.
              
              its located in syscall.c
              and it also returns a status. i assume that if status code is -1 it didnt work well.
              
              there are examples for argint also up here. all of them call the arguments
              and put them inside the address given.
              more in sysfile.c.
*/
int 
sys_setvar(void)
{
  char* varName ="hello";
  char* value = "there";
  
  // cprintf("[DEBUG] sys_setvar: Initial values: <varName: %s, value: %s>\n" , varName, value);
  if((argstr(0, &varName) < 0) || (argstr(1 , &value) < 0)){
    // cprintf("[DEBUG] sys_setvar: Problem retrieving arguments\n");
    return -2; //-2 – Input is illegal. we should also take care of it in the function itself!
  }
  return setvar(varName , value);
}

//AVI
int 
sys_getvar(void)
{
  char* varName ="hello";
  char* value = "there";
  
  // cprintf("[DEBUG] sys_getvar: Initial values: <varName: %s, value: %s>\n" , varName, value);
  if((argstr(0, &varName) < 0) || (argstr(1 , &value) < 0)){
    // cprintf("[DEBUG] sys_getvar: Problem retrieving arguments\n");
    return -2; //-2 – Input is illegal. we should also take care of it in the function itself!
  }
  return getvar(varName , value);
}

//AVI
int 
sys_remvar(void)
{
  char* varName ="hello";
  
  // cprintf("[DEBUG] sys_remvar: Initial values: <varName: %s>\n" , varName);
  if(argstr(0, &varName) < 0){
    // cprintf("[DEBUG] sys_remvar: Problem retrieving arguments\n");
    return -2; //-2 – Input is illegal. we should also take care of it in the function itself!
  }
  return remvar(varName);
}

// Updates the processes timers //AVI
void
sys_timersupdate(void)
{
  return timersupdate();
}

// wait2 system call, collects runtime data for given process //AVI
int
sys_wait2(void)
{
  int pid = 0;
  int* wtime = 0;
  int* rtime = 0;
  int* iotime = 0;
  
  // cprintf("[DEBUG] sys_wait2: im here\n");
  if((argint(0, &pid) < 0) || (argptr(1 , (void*) &wtime, 4) < 0) || (argptr(2, (void*) &rtime, 4) < 0) || (argptr(3, (void*) &iotime, 4) < 0)) { //TODO
    return -2; //-2 – Input is illegal.
  }
  return wait2(pid, wtime, rtime, iotime);
}

int 
sys_setpriority(void)
{
  int priority = 0;

  // cprintf("[DEBUG] sys_setvar: Initial values: <varName: %s, value: %s>\n" , varName, value);
  if(argint(0, &priority) < 0){
    // cprintf("[DEBUG] sys_setvar: Problem retrieving arguments\n");
    return -1; //-1 – Input is illegal. we should also take care of it in the function itself!
  }
  return setpriority(priority); 
}