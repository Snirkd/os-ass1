#include "types.h" 
#include "stat.h" 
#include "user.h" 
#include "fcntl.h" 

// mandatory for the new system call so it can run on a new "forked" process. 
// this file is the application itself.
int
main(int argc, char *argv[])
{
	char* varName;
	char* value;

	if(argc < 2){
    	printf(2, "Too few arguments for getvar...\n");
    	exit();
  	}

  	varName = argv[1];
  	value = malloc(128);
	
	int getres = getvar(varName , value);

	if(getres == 0) {
		printf(2, "The value of %s is: %s\n", varName, value);
	} else {
		printf(2, "No variable with the name of %s\n", varName);
	}

	free(value);

	exit();
}