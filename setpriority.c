#include "types.h" 
#include "stat.h" 
#include "user.h" 
#include "fcntl.h" 

// mandatory for the new system call so it can run on a new "forked" process. 
// this file is the application itself.
int
main(int argc, char *argv[])
{
	int priority = 0;

	if(argc < 2){
    	printf(2, "Too few arguments for setpriority...\n");
    	exit();
  	}

  	priority = atoi(argv[1]);

	setpriority(priority);

	exit();
}