#include "types.h" 
#include "stat.h" 
#include "user.h" 
#include "fcntl.h" 

// mandatory for the new system call so it can run on a new "forked" process. 
// this file is the application itself.
int
main(int argc, char *argv[])
{
	char* varName;

	if(argc < 2){
    	printf(2, "Too few arguments for getvar...\n");
    	exit();
  	}

  	varName = argv[1];
	
	int getres = remvar(varName);

	if(getres == 0) {
		printf(2, "Removed global variable: %s\n", varName);
	} else {
		printf(2, "No variable with the name of %s\n", varName);
	}

	exit();
}