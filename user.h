struct stat;
struct rtcdate;

// system calls
int fork(void);
int exit(void) __attribute__((noreturn));
int wait(void);
int pipe(int*);
int write(int, void*, int);
int read(int, void*, int);
int close(int);
int kill(int);
int exec(char*, char**);
int open(char*, int);
int mknod(char*, short, short);
int unlink(char*);
int fstat(int fd, struct stat*);
int link(char*, char*);
int mkdir(char*);
int chdir(char*);
int dup(int);
int getpid(void);
char* sbrk(int);
int sleep(int);
int uptime(void);
int yield(void);
int cps(void);
int setvar(char*, char*);
int getvar(char*, char*); //AVI 1.2.2 new syscall. note that only type is entered here, without arugment names.
int remvar(char*); //AVI 1.2.3 new syscall. note that only type is entered here, without arugment names.
void timersupdate(void); //AVI 2 a syscall that updates the processes timers
int wait2(int, int*, int*, int*); //AVI 2 wait2 syscall
int setpriority(int); // SNIR3.4

// ulib.c
int stat(char*, struct stat*);
char* strcpy(char*, char*);
void *memmove(void*, void*, int);
char* strchr(const char*, char c);
char* strpbrk(const char *str, const char *delimiters); //AVI
int strcmp(const char*, const char*);
void printf(int, char*, ...);
char* gets(char*, int max);
uint strlen(char*);
void* memset(void*, int, uint);
void* malloc(uint);
void free(void*);
int atoi(const char*);
