// A simple program that just prints stuff on screen

#include "types.h"
#include "stat.h"
#include "user.h"

int
main(void)
{
	printf(1, "SchedSanity program\n");

	int calcSmall[3];
	int calcLarge[3];
	int ioSmall[3];
	int ioLarge[3];

	int i = 0;

	// calcSmall
	for(i = 0; i < 3; i++){
		calcSmall[i] = fork();

		#ifdef CFSD
			setpriority(i+1);
		#endif

		if(calcSmall[i] == 0){
			int l = 0;
			int sigma = 0;
			for(l = 0; l < 1000; l++){
				sigma += 5;
			}
			exit();
		} else {
			// wait();
		}
	}

	// calcLarge
	for(i = 0; i < 3; i++){
		calcLarge[i] = fork();

		#ifdef CFSD
			setpriority(i+1);
		#endif

		if(calcLarge[i] == 0){
			int l = 0;
			int sigma = 0;
			for(l = 0; l < 10000; l++){
				sigma += 5;
			}
			exit();
		} else {
			// wait();
		}
	}

	// ioSmall
	for(i = 0; i < 3; i++){
		ioSmall[i] = fork();

		#ifdef CFSD
			setpriority(i+1);
		#endif

		if(ioSmall[i] == 0){
			int l = 0;
			for(l = 0; l < 1000; l++){
				printf(1, "Vadim drinks %d vodkas\n", l);
			}
			exit();
		} else {
			// wait();
		}
	}

	// ioLarge
	for(i = 0; i < 3; i++){
		ioLarge[i] = fork();

		#ifdef CFSD
			setpriority(i+1);
		#endif

		if(ioLarge[i] == 0){
			int l = 0;
			for(l = 0; l < 10000; l++){
				printf(1, "Vadim drinks %d vodkas\n", l);
			}
			exit();
		} else {
			// wait();
		}
	}



	

	int CSwtime = 0, CSrtime = 0, CSiotime = 0;
	for(i = 0; i < 3; i++){
		int wtime = 0;
		int rtime = 0;
		int iotime = 0;

		int wait2res = wait2(calcSmall[i], &wtime, &rtime, &iotime);

		if(wait2res == -1){
			printf(1, "Something went wrong!\n");
			exit();
		}

		CSwtime += wtime;
		CSrtime += rtime;
		CSiotime += iotime;
	}

	int CLwtime = 0, CLrtime = 0, CLiotime = 0;
	for(i = 0; i < 3; i++){
		int wtime = 0;
		int rtime = 0;
		int iotime = 0;

		int wait2res = wait2(calcLarge[i], &wtime, &rtime, &iotime);

		if(wait2res == -1){
			printf(1, "Something went wrong!\n");
			exit();
		}

		CLwtime += wtime;
		CLrtime += rtime;
		CLiotime += iotime;
	}

	int IOSwtime = 0, IOSrtime = 0, IOSiotime = 0;
	for(i = 0; i < 3; i++){
		int wtime = 0;
		int rtime = 0;
		int iotime = 0;

		int wait2res = wait2(ioSmall[i], &wtime, &rtime, &iotime);

		if(wait2res == -1){
			printf(1, "Something went wrong!\n");
			exit();
		}

		IOSwtime += wtime;
		IOSrtime += rtime;
		IOSiotime += iotime;
	}

	int IOLwtime = 0, IOLrtime = 0, IOLiotime = 0;
	for(i = 0; i < 3; i++){
		int wtime = 0;
		int rtime = 0;
		int iotime = 0;

		int wait2res = wait2(ioLarge[i], &wtime, &rtime, &iotime);

		if(wait2res == -1){
			printf(1, "Something went wrong!\n");
			exit();
		}

		IOLwtime += wtime;
		IOLrtime += rtime;
		IOLiotime += iotime;
	}

	#ifdef DEFAULT
		printf(1, "DEFAULT run statistics:\n");
	#endif

	#ifdef FCFS
		printf(1, "FCFS run statistics:\n");
	#endif

	#ifdef SRT
		printf(1, "SRT run statistics:\n");
	#endif

	#ifdef CFSD
		printf(1, "CFSD run statistics:\n");
	#endif

	CSwtime = CSwtime/3;
	CSrtime = CSrtime/3;
	CSiotime = CSiotime/3;

	printf(1, "Small Calc stats:\naverage wtime: %d, average iotime: %d, average rtime: %d\n", CSwtime, CSrtime, CSiotime);

	CLwtime = CLwtime/3;
	CLrtime = CLrtime/3;
	CLiotime = CLiotime/3;

	printf(1, "Large Calc stats:\naverage wtime: %d, average iotime: %d, average rtime: %d\n", CLwtime, CLrtime, CLiotime);

	IOSwtime = IOSwtime/3;
	IOSrtime = IOSrtime/3;
	IOSiotime = IOSiotime/3;

	printf(1, "Small IO stats:\naverage wtime: %d, average iotime: %d, average rtime: %d\n", IOSwtime, IOSrtime, IOSiotime);

	IOLwtime = IOLwtime/3;
	IOLrtime = IOLrtime/3;
	IOLiotime = IOLiotime/3;

	printf(1, "Large IO stats:\naverage wtime: %d, average iotime: %d, average rtime: %d\n", IOLwtime, IOLrtime, IOLiotime);


	exit();
}