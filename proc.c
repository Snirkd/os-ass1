#include "types.h"
#include "defs.h"
#include "param.h"
#include "memlayout.h"
#include "mmu.h"
#include "x86.h"
#include "proc.h"
#include "spinlock.h"

/* Definitions For Global vars */
#define MAX_VARIABLES 32 // max global vars num
#define MAX_GLOBVAR_NAME_LENGTH 32 // max length of global var name. only a-z and A-Z allowed!
#define MAX_GLOBVAR_VALUE_LENGTH 128 // max length of global var. all chars are allowed here.
#define ACTUAL_MAX_GLOBVAR_NAME_LENGTH (MAX_GLOBVAR_NAME_LENGTH + 1) // for the null terminator
#define ACTUAL_MAX_GLOBVAR_VALUE_LENGTH (MAX_GLOBVAR_VALUE_LENGTH + 1) // for the null terminator

uint rdpriority = 0; //SNIR3: FOR FCFS 

/* char* variable – A pointer to a buffer that will hold the name of the variable. Assume max
   buffer size of 32, containing English characters only (a-z, A-Z).
   char* value – A pointer to a buffer that will hold the value assigned to the variable.
   Assume max buffer size of 128, which may contain any character.*/
struct globvar{
  char variable[ACTUAL_MAX_GLOBVAR_NAME_LENGTH];
  char value[ACTUAL_MAX_GLOBVAR_VALUE_LENGTH]; 
};

/* Struct for the global vars */
struct {
  struct spinlock lock; // to lock on when writing/reading a var.
  struct globvar globvars[MAX_VARIABLES]; // array of global var structures.
  int currsize;
} globvartable;

/* Definitions For Global vars */

struct {
  struct spinlock lock;
  struct proc proc[NPROC];
} ptable;

static struct proc *initproc;

int nextpid = 1;
extern void forkret(void);
extern void trapret(void);

static void wakeup1(void *chan);

void
pinit(void)
{
  initlock(&ptable.lock, "ptable");
}

// Must be called with interrupts disabled
int
cpuid() {
  return mycpu()-cpus;
}

// Must be called with interrupts disabled to avoid the caller being
// rescheduled between reading lapicid and running through the loop.
struct cpu*
mycpu(void)
{
  int apicid, i;
  
  if(readeflags()&FL_IF)
    panic("mycpu called with interrupts enabled\n");
  
  apicid = lapicid();
  // APIC IDs are not guaranteed to be contiguous. Maybe we should have
  // a reverse map, or reserve a register to store &cpus[i].
  for (i = 0; i < ncpu; ++i) {
    if (cpus[i].apicid == apicid)
      return &cpus[i];
  }
  panic("unknown apicid\n");
}

// Disable interrupts so that we are not rescheduled
// while reading proc from the cpu structure
struct proc*
myproc(void) {
  struct cpu *c;
  struct proc *p;
  pushcli();
  c = mycpu();
  p = c->proc;
  popcli();
  return p;
}

//PAGEBREAK: 32
// Look in the process table for an UNUSED proc.
// If found, change state to EMBRYO and initialize
// state required to run in the kernel.
// Otherwise return 0.
static struct proc*
allocproc(void)
{
  struct proc *p;
  char *sp;

  acquire(&ptable.lock);

  for(p = ptable.proc; p < &ptable.proc[NPROC]; p++)
    if(p->state == UNUSED)
      goto found;

  release(&ptable.lock);
  return 0;

found:
  p->state = EMBRYO;
  p->pid = nextpid++;

  release(&ptable.lock);

  // Allocate kernel stack.
  if((p->kstack = kalloc()) == 0){
    p->state = UNUSED;
    return 0;
  }
  sp = p->kstack + KSTACKSIZE;

  // Leave room for trap frame.
  sp -= sizeof *p->tf;
  p->tf = (struct trapframe*)sp;

  // Set up new context to start executing at forkret,
  // which returns to trapret.
  sp -= 4;
  *(uint*)sp = (uint)trapret;

  sp -= sizeof *p->context;
  p->context = (struct context*)sp;
  memset(p->context, 0, sizeof *p->context);
  p->context->eip = (uint)forkret;

  return p;
}

//PAGEBREAK: 32
// Set up first user process.
void
userinit(void)
{
  struct proc *p;
  extern char _binary_initcode_start[], _binary_initcode_size[];

  p = allocproc();
  
  initproc = p;
  if((p->pgdir = setupkvm()) == 0)
    panic("userinit: out of memory?");
  inituvm(p->pgdir, _binary_initcode_start, (int)_binary_initcode_size);
  p->sz = PGSIZE;
  memset(p->tf, 0, sizeof(*p->tf));
  p->tf->cs = (SEG_UCODE << 3) | DPL_USER;
  p->tf->ds = (SEG_UDATA << 3) | DPL_USER;
  p->tf->es = p->tf->ds;
  p->tf->ss = p->tf->ds;
  p->tf->eflags = FL_IF;
  p->tf->esp = PGSIZE;
  p->tf->eip = 0;  // beginning of initcode.S

  safestrcpy(p->name, "initcode", sizeof(p->name));
  p->cwd = namei("/");

  // this assignment to p->state lets other cores
  // run this process. the acquire forces the above
  // writes to be visible, and the lock is also needed
  // because the assignment might not be atomic.
  acquire(&ptable.lock);
  p->timeInCurrQuantum = 0; //SNIR3 reset current quantum timer.
  p->rdtime = rdpriority++; //SNIR3 FOR FCFS: will be used for priority.
  p->priority = 2;
  p->state = RUNNABLE;

  release(&ptable.lock);
}

// Grow current process's memory by n bytes.
// Return 0 on success, -1 on failure.
int
growproc(int n)
{
  uint sz;
  struct proc *curproc = myproc();

  sz = curproc->sz;
  if(n > 0){
    if((sz = allocuvm(curproc->pgdir, sz, sz + n)) == 0)
      return -1;
  } else if(n < 0){
    if((sz = deallocuvm(curproc->pgdir, sz, sz + n)) == 0)
      return -1;
  }
  curproc->sz = sz;
  switchuvm(curproc);
  return 0;
}

// Create a new process copying p as the parent.
// Sets up stack to return as if from system call.
// Caller must set state of returned proc to RUNNABLE.
int
fork(void)
{
  int i, pid;
  struct proc *np;
  struct proc *curproc = myproc();

  // Allocate process.
  if((np = allocproc()) == 0){
    return -1;
  }

  // Copy process state from proc.
  if((np->pgdir = copyuvm(curproc->pgdir, curproc->sz)) == 0){
    kfree(np->kstack);
    np->kstack = 0;
    np->state = UNUSED;
    return -1;
  }
  np->sz = curproc->sz;
  np->parent = curproc;
  *np->tf = *curproc->tf;

  np->ctime = ticks; // set the creation time for that process //AVI
  np->etime = 0;     // set the end time time for that process //AVI
  np->iotime = 0;    // set the total time waiting for I/O for that process //AVI
  np->rtime = 0;     // set the total running time for that process //AVI

  // Clear %eax so that fork returns 0 in the child.
  np->tf->eax = 0;

  for(i = 0; i < NOFILE; i++)
    if(curproc->ofile[i])
      np->ofile[i] = filedup(curproc->ofile[i]);
  np->cwd = idup(curproc->cwd);

  safestrcpy(np->name, curproc->name, sizeof(curproc->name));

  pid = np->pid;

  acquire(&ptable.lock);
  np->timeInCurrQuantum = 0; //SNIR3 reset current quantum timer.
  np->rdtime = rdpriority++; //SNIR3 FOR FCFS: will be used for priority.
  np->approxRunTime_i = QUANTUM; //SNIR3
  np->priority = curproc->priority; //SNIR3.4
  np->state = RUNNABLE;

  release(&ptable.lock);

  return pid;
}

// Exit the current process.  Does not return.
// An exited process remains in the zombie state
// until its parent calls wait() to find out it exited.
void
exit(void)
{
  struct proc *curproc = myproc();
  struct proc *p;
  int fd;

  if(curproc == initproc)
    panic("init exiting");

  // Close all open files.
  for(fd = 0; fd < NOFILE; fd++){
    if(curproc->ofile[fd]){
      fileclose(curproc->ofile[fd]);
      curproc->ofile[fd] = 0;
    }
  }

  begin_op();
  iput(curproc->cwd);
  end_op();
  curproc->cwd = 0;

  acquire(&ptable.lock);

  // Parent might be sleeping in wait().
  wakeup1(curproc->parent);

  // Pass abandoned children to init.
  for(p = ptable.proc; p < &ptable.proc[NPROC]; p++){
    if(p->parent == curproc){
      p->parent = initproc;
      if(p->state == ZOMBIE)
        wakeup1(initproc);
    }
  }

  // Jump into the scheduler, never to return.
  int t = ticks; //AVI
  // cprintf("[DEBUG] t: %d\n", t); //AVI
  curproc->state = ZOMBIE; //AVI
  curproc->etime = t; //AVI
  sched();
  panic("zombie exit");
}

// Wait for a child process to exit and return its pid.
// Return -1 if this process has no children.
int
wait(void)
{
  struct proc *p;
  int havekids, pid;
  struct proc *curproc = myproc();
  
  acquire(&ptable.lock);
  for(;;){
    // Scan through table looking for exited children.
    havekids = 0;
    for(p = ptable.proc; p < &ptable.proc[NPROC]; p++){
      if(p->parent != curproc)
        continue;
      havekids = 1;
      if(p->state == ZOMBIE){
        // Found one.
        pid = p->pid;
        kfree(p->kstack);
        p->kstack = 0;
        freevm(p->pgdir);
        p->pid = 0;
        p->parent = 0;
        p->name[0] = 0;
        p->killed = 0;
        p->state = UNUSED;
        release(&ptable.lock);
        return pid;
      }
    }

    // No point waiting if we don't have any children.
    if(!havekids || curproc->killed){
      release(&ptable.lock);
      return -1;
    }

    // Wait for children to exit.  (See wakeup1 call in proc_exit.)
    sleep(curproc, &ptable.lock);  //DOC: wait-sleep
  }
}

//PAGEBREAK: 42
// Per-CPU process scheduler.
// Each CPU calls scheduler() after setting itself up.
// Scheduler never returns.  It loops, doing:
//  - choose a process to run
//  - swtch to start running that process
//  - eventually that process transfers control
//      via swtch back to the scheduler.
void
scheduler(void)
{
  struct proc *p;
  struct cpu *c = mycpu();
  c->proc = 0;
  
  for(;;){
    // Enable interrupts on this processor.
    sti();

    // Default Scheduler policy:
    #ifdef DEFAULT
    // Loop over process table looking for process to run.
    acquire(&ptable.lock);
    for(p = ptable.proc; p < &ptable.proc[NPROC]; p++){
      if(p->state != RUNNABLE)
        continue;

      // Switch to chosen process.  It is the process's job
      // to release ptable.lock and then reacquire it
      // before jumping back to us.
      c->proc = p;
      switchuvm(p);
      p->state = RUNNING;

      swtch(&(c->scheduler), p->context);
      switchkvm();

      // Process is done running for now.
      // It should have changed its p->state before coming back.
      c->proc = 0;
    }
    release(&ptable.lock);
    #endif

    // FCFS (First Come First Serve) Scheduler policy: SNIR3
    // This is a non-preemptive policy. A process that gets the CPU runs until it no longer needs it (yield / finish /
    // blocked). Yielding the CPU forces the process to the end of the line.
    #ifdef FCFS
    acquire(&ptable.lock);

    // selecting a RUNNABLE process with the minimum rdtime:
    struct proc* currproc = ptable.proc; //SNIR3 initialize to be the first proc. 
    uint currmin = rdpriority;
    
    for(p = ptable.proc; p < &ptable.proc[NPROC]; p++){
      if(p->state != RUNNABLE)
        continue;

      if(p->rdtime < currmin){
        currproc = p;
        currmin = p->rdtime;
      } 
    }

    // Set p to the proc with the lowest rdtime that we found
    p = currproc;

    if(p->state == RUNNABLE){
      // Switch to chosen process.  It is the process's job
      // to release ptable.lock and then reacquire it
      // before jumping back to us.
      c->proc = p;
      switchuvm(p);
      p->state = RUNNING;

      swtch(&(c->scheduler), p->context);
      switchkvm();

      // Process is done running for now.
      // It should have changed its p->state before coming back.
      c->proc = 0;
    }
    release(&ptable.lock);
    #endif

    // SRT (Shortest Remaining Time) Scheduler policy:
    #ifdef SRT
      acquire(&ptable.lock);
      double currmin = QUANTUM;
      struct proc *currproc = 0;
      for(p = ptable.proc; p < &ptable.proc[NPROC]; p++){
        if(p->state == RUNNABLE){
          if(currproc != 0){
            if(p->approxRunTime_i < currmin){
              currproc = p;
              currmin = p->approxRunTime_i;
            }
          }
          else{
            currproc = p;
            currmin = p->approxRunTime_i;
          }
        }
      }

    if(currproc != 0){
      // Switch to chosen process.  It is the process's job
      // to release ptable.lock and then reacquire it
      // before jumping back to us.
      p = currproc;
      c->proc = p;
      switchuvm(p);
      p->state = RUNNING;

      swtch(&(c->scheduler), p->context);
      switchkvm();

      // Process is done running for now.
      // It should have changed its p->state before coming back.
      c->proc = 0;
    }

    release(&ptable.lock);
    #endif

    // CFSD (Completely Fair Schedular with Priority decay) Scheduler policy:
    #ifdef CFSD
    acquire(&ptable.lock);
    double currmin = 0;
    struct proc *currproc = 0;
    for(p = ptable.proc; p < &ptable.proc[NPROC]; p++){
      if(p->state == RUNNABLE){
        if(currproc != 0){
          int wtime = ticks - p->ctime - p->iotime - p->rtime;
          double currRunTimeRatio = 0;
          double decayfactor = 1;
          switch(p->priority){
            case 1: //High priority
              decayfactor = HIGHPRIORITY_DECAYFACTOR;
              break;
            case 2: //Normal priority
              decayfactor = NORMALPRIORITY_DECAYFACTOR;              
              break;
            case 3: //Low priority
              decayfactor = LOWPRIORITY_DECAYFACTOR;
              break;
          }
          currRunTimeRatio = (p->rtime * decayfactor) / (p->rtime + wtime);

          if(currRunTimeRatio < currmin){
            currproc = p;
            currmin = currRunTimeRatio;
          }
        }
        else{
          int wtime = ticks - p->ctime - p->iotime - p->rtime;
          double currRunTimeRatio = 0;
          double decayfactor = 1;
          switch(p->priority){
            case 1: //High priority
              decayfactor = HIGHPRIORITY_DECAYFACTOR;
              break;
            case 2: //Normal priority
              decayfactor = NORMALPRIORITY_DECAYFACTOR;              
              break;
            case 3: //Low priority
              decayfactor = LOWPRIORITY_DECAYFACTOR;              
              break;
          }
          currRunTimeRatio = (p->rtime * decayfactor) / (p->rtime + wtime);
          currproc = p;
          currmin = currRunTimeRatio;
        }
      }
    }

    if(currproc != 0){
      // Switch to chosen process.  It is the process's job
      // to release ptable.lock and then reacquire it
      // before jumping back to us.
      p = currproc;
      c->proc = p;
      switchuvm(p);
      p->state = RUNNING;

      swtch(&(c->scheduler), p->context);
      switchkvm();

      // Process is done running for now.
      // It should have changed its p->state before coming back.
      c->proc = 0;
    }

    release(&ptable.lock);    
    #endif
  }
}

// Enter scheduler.  Must hold only ptable.lock
// and have changed proc->state. Saves and restores
// intena because intena is a property of this
// kernel thread, not this CPU. It should
// be proc->intena and proc->ncli, but that would
// break in the few places where a lock is held but
// there's no process.
void
sched(void)
{
  int intena;
  struct proc *p = myproc();

  if(!holding(&ptable.lock))
    panic("sched ptable.lock");
  if(mycpu()->ncli != 1)
    panic("sched locks");
  if(p->state == RUNNING)
    panic("sched running");
  if(readeflags()&FL_IF)
    panic("sched interruptible");
  intena = mycpu()->intena;
  swtch(&p->context, mycpu()->scheduler);
  mycpu()->intena = intena;
}

// Give up the CPU for one scheduling round.
void
yield(void)
{
  acquire(&ptable.lock);  //DOC: yieldlock
  myproc()->timeInCurrQuantum = 0; //SNIR3 reset current quantum timer (with every yield)   
  myproc()->rdtime = rdpriority++; //SNIR3 FOR FCFS: will be used for priority.
  if(myproc()->rtime >= QUANTUM){
    myproc()->approxRunTime_i = (1 + ALPHA) * myproc()->approxRunTime_i;
  }
  myproc()->state = RUNNABLE;
  sched();
  release(&ptable.lock);
}

// A fork child's very first scheduling by scheduler()
// will swtch here.  "Return" to user space.
void
forkret(void)
{
  static int first = 1;
  // Still holding ptable.lock from scheduler.
  release(&ptable.lock);

  if (first) {
    // Some initialization functions must be run in the context
    // of a regular process (e.g., they call sleep), and thus cannot
    // be run from main().
    first = 0;
    iinit(ROOTDEV);
    initlog(ROOTDEV);
  }

  // Return to "caller", actually trapret (see allocproc).
}

// Atomically release lock and sleep on chan.
// Reacquires lock when awakened.
void
sleep(void *chan, struct spinlock *lk)
{
  struct proc *p = myproc();
  
  if(p == 0)
    panic("sleep");

  if(lk == 0)
    panic("sleep without lk");

  // Must acquire ptable.lock in order to
  // change p->state and then call sched.
  // Once we hold ptable.lock, we can be
  // guaranteed that we won't miss any wakeup
  // (wakeup runs with ptable.lock locked),
  // so it's okay to release lk.
  if(lk != &ptable.lock){  //DOC: sleeplock0
    acquire(&ptable.lock);  //DOC: sleeplock1
    release(lk);
  }
  // Go to sleep.
  p->chan = chan;
  p->state = SLEEPING;
  if(p->rtime >= QUANTUM){
    p->approxRunTime_i = (1 + ALPHA) * p->approxRunTime_i;
  }

  sched();

  // Tidy up.
  p->chan = 0;

  // Reacquire original lock.
  if(lk != &ptable.lock){  //DOC: sleeplock2
    release(&ptable.lock);
    acquire(lk);
  }
}

//PAGEBREAK!
// Wake up all processes sleeping on chan.
// The ptable lock must be held.
static void
wakeup1(void *chan)
{
  struct proc *p;

  for(p = ptable.proc; p < &ptable.proc[NPROC]; p++)
    if(p->state == SLEEPING && p->chan == chan){
      p->timeInCurrQuantum = 0; //SNIR3 reset current quantum timer. 
      p->rdtime = rdpriority++; //SNIR3 FOR FCFS: will be used for priority.           
      p->state = RUNNABLE;
    }
}

// Wake up all processes sleeping on chan.
void
wakeup(void *chan)
{
  acquire(&ptable.lock);
  wakeup1(chan);
  release(&ptable.lock);
}

// Kill the process with the given pid.
// Process won't exit until it returns
// to user space (see trap in trap.c).
int
kill(int pid)
{
  struct proc *p;

  acquire(&ptable.lock);
  for(p = ptable.proc; p < &ptable.proc[NPROC]; p++){
    if(p->pid == pid){
      p->killed = 1;
      // Wake process from sleep if necessary.
      if(p->state == SLEEPING){
        p->timeInCurrQuantum = 0; //SNIR3 reset current quantum timer. 
        p->rdtime = rdpriority++; //SNIR3 FOR FCFS: will be used for priority.
        p->state = RUNNABLE;
      }
      release(&ptable.lock);
      return 0;
    }
  }
  release(&ptable.lock);
  return -1;
}

//PAGEBREAK: 36
// Print a process listing to console.  For debugging.
// Runs when user types ^P on console.
// No lock to avoid wedging a stuck machine further.
void
procdump(void)
{
  static char *states[] = {
  [UNUSED]    "unused",
  [EMBRYO]    "embryo",
  [SLEEPING]  "sleep ",
  [RUNNABLE]  "runble",
  [RUNNING]   "run   ",
  [ZOMBIE]    "zombie"
  };
  int i;
  struct proc *p;
  char *state;
  uint pc[10];

  for(p = ptable.proc; p < &ptable.proc[NPROC]; p++){
    if(p->state == UNUSED)
      continue;
    if(p->state >= 0 && p->state < NELEM(states) && states[p->state])
      state = states[p->state];
    else
      state = "???";
    cprintf("%d %s %s", p->pid, state, p->name);
    if(p->state == SLEEPING){
      getcallerpcs((uint*)p->context->ebp+2, pc);
      for(i=0; i<10 && pc[i] != 0; i++)
        cprintf(" %p", pc[i]);
    }
    cprintf("\n");
  }
}

// current process status: //AVI
int
cps()
{
  struct proc *p;

  // Enable interrupts on this processor.
  sti();

  // Loop over process table and look for process with pid.
  acquire(&ptable.lock);
  cprintf("name \t pid \t ctime \t etime \t iotime\t rtime \t rdtime \t priority \t state \t\n");
  for(p = ptable.proc; p < &ptable.proc[NPROC] ; p++){
    if(p->state == SLEEPING)
      cprintf("%s \t %d \t %d \t %d \t %d \t %d \t %d \t %d \t SLEEPING \t \n" , p->name , p->pid, p->ctime, p->etime, p->iotime, p->rtime, p->rdtime, p->priority); // cprintf allows printing from kernel mode.
    else if(p->state == RUNNING)
      cprintf("%s \t %d \t %d \t %d \t %d \t %d \t %d \t %d \t RUNNING \t \n" , p->name , p->pid, p->ctime, p->etime, p->iotime, p->rtime, p->rdtime, p->priority); // cprintf allows printing from kernel mode.
    else if(p->state == RUNNABLE)
      cprintf("%s \t %d \t %d \t %d \t %d \t %d \t %d \t %d \t RUNNABLE \t \n" , p->name , p->pid, p->ctime, p->etime, p->iotime, p->rtime, p->rdtime, p->priority); // cprintf allows printing from kernel mode.
  }

  release(&ptable.lock);

  return 23;
}

// function that prints the global var table, for debug purposes.
void
printglobvartable()
{
  cprintf("[DEBUG] Printing global var table: \n"); // cprintf allows printing from kernel mode.
  cprintf("[DEBUG] table size is: %d\n" ,globvartable.currsize);
  int i = 0;
  for(i = 0; i < globvartable.currsize ; i++){
      cprintf("[DEBUG] #%d: %s = %s\n", i ,globvartable.globvars[i].variable , globvartable.globvars[i].value);
  }
  cprintf("[DEBUG] Printed global var table. \n"); // cprintf allows printing from kernel mode.
}

// AVI only english letters in string checker
int
checkIfOnlyEnglishLetters(char* string, int stringlength)
{
  int i = 0;
  for(i = 0; i < stringlength; i++)
  {
    if(string[i] < 'A' ||
       (string[i] > 'Z' && string[i] < 'a') ||
       string[i] > 'z') {
      return -1;
    }
  }
  return 0;
}

// setvar logics according to assignment specs
int
setvar(char* varName, char* value)
{
  // Enable interrupts on this processor.
  sti();

  // get the lock for globvartable so no other cpu can run and override it.
  acquire(&globvartable.lock);

  // cprintf("[DEBUG] GOOD, IT RUNS! (inside setvar) \n"); // cprintf allows printing from kernel mode.
  // cprintf("[DEBUG] params:\n");
  // cprintf("[DEBUG] varName: %s\n", varName);
  // cprintf("[DEBUG] value: %s\n", value);

  // Case 1: illegal var or val size:
  if(strlen(varName) > MAX_GLOBVAR_NAME_LENGTH)
  {
    // cprintf("[DEBUG] the variable name length of %d is too long (%s)\n", strlen(varName), varName);

    release(&globvartable.lock);
    return -2;
  }
  if(strlen(value) > MAX_GLOBVAR_VALUE_LENGTH)
  {
    // cprintf("[DEBUG] the value length of %d is too long (%s)\n", strlen(value), value);

    release(&globvartable.lock);
    return -2;
  }
  if(checkIfOnlyEnglishLetters(varName, strlen(varName)) == -1)
  {
    // cprintf("[DEBUG] the variable name does not consist of only english letters (%s)\n", varName);

    release(&globvartable.lock);
    return -2;
  }

  // Case 2: var exists in table, override value:
  int i = 0;
  for(i = 0; i < globvartable.currsize; i++){
    if(strlen(globvartable.globvars[i].variable) == strlen(varName) && strncmp(globvartable.globvars[i].variable, varName, strlen(varName)) == 0){
      // cprintf("[DEBUG] following value is already in table: %s\n", varName);

      strncpy(globvartable.globvars[i].value, value, strlen(value));

      // printglobvartable();

      release(&globvartable.lock);
      return 0;
    }
  }

  // Case 3: fresh value:
  if(globvartable.currsize < MAX_VARIABLES){
    strncpy(globvartable.globvars[globvartable.currsize].variable, varName, strlen(varName));
    strncpy(globvartable.globvars[globvartable.currsize].value, value, strlen(value));

    globvartable.currsize = globvartable.currsize + 1;
    // printglobvartable();

    release(&globvartable.lock);  
    return 0;
  } else {
    // Case 4: no room in table:

    release(&globvartable.lock);
    return -1;
  }
}

//AVI getvar logics according to assignment specs
int
getvar(char* varName, char* value)
{
  // Enable interrupts on this processor.
  sti();

  // get the lock for globvartable so no other cpu can run and override it.
  acquire(&globvartable.lock);

  // cprintf("[DEBUG] getvar: GOOD, IT RUNS!\n"); // cprintf allows printing from kernel mode.
  // cprintf("[DEBUG] getvar: params:\n");
  // cprintf("[DEBUG] getvar: varName: %s\n", varName);
  // cprintf("[DEBUG] getvar: value: %s\n", value);

  int i = 0;
  for(i = 0; i < globvartable.currsize; i++){
    if(strlen(globvartable.globvars[i].variable) == strlen(varName) && strncmp(globvartable.globvars[i].variable, varName, strlen(varName)) == 0){
      // cprintf("[DEBUG] getvar: following value is in table: %s\n", varName);

      strncpy(value, globvartable.globvars[i].value, strlen(globvartable.globvars[i].value));

      release(&globvartable.lock);
      return 0;
    }
  }

  release(&globvartable.lock);
  return -1;
}

//AVI remvar logics according to assignment specs
int
remvar(char* varName)
{
  // Enable interrupts on this processor.
  sti();

  // get the lock for globvartable so no other cpu can run and override it.
  acquire(&globvartable.lock);

  // cprintf("[DEBUG] getvar: GOOD, IT RUNS!\n"); // cprintf allows printing from kernel mode.
  // cprintf("[DEBUG] getvar: params:\n");
  // cprintf("[DEBUG] getvar: varName: %s\n", varName);

  int i = 0;
  for(i = 0; i < globvartable.currsize; i++){
    if(strlen(globvartable.globvars[i].variable) == strlen(varName) && strncmp(globvartable.globvars[i].variable, varName, strlen(varName)) == 0){
      // cprintf("[DEBUG] remvar: following value is in table: %s\n", varName);

      // simply copying all of the next variables 1 cell back, and setting the last variable to nothing.
      int j = i;
      for(j = i; j < globvartable.currsize-1; j++){
        strncpy(globvartable.globvars[j].variable, globvartable.globvars[j+1].variable, strlen(globvartable.globvars[j+1].variable));
        strncpy(globvartable.globvars[j].value, globvartable.globvars[j+1].value, strlen(globvartable.globvars[j+1].value));
      }
      memset(globvartable.globvars[j].variable, 0, strlen(globvartable.globvars[j].variable));
      memset(globvartable.globvars[j].value, 0, strlen(globvartable.globvars[j].value));
      globvartable.currsize--;

      // printglobvartable();

      release(&globvartable.lock);
      return 0;
    }
  }

  // printglobvartable();

  release(&globvartable.lock);
  return -1;
}

// Update the timers of the processes //AVI
void
timersupdate()
{
  struct proc *p;

  acquire(&ptable.lock);
  for(p = ptable.proc; p < &ptable.proc[NPROC]; p++){
    if(p->state == SLEEPING){
      p->iotime++;
    } else if(p->state == RUNNING){
      p->rtime++;
      p->timeInCurrQuantum++;
    }
  }
  release(&ptable.lock);
}

//AVI
// Wait for a process with pid "pid" to exit and insert into the pointers perfomence info.
// Return -1 if the process with pid "pid" does not exist.
int
wait2(int pid, int* wtime, int* rtime, int* iotime)
{
  struct proc *p;
  int foundpid;
  struct proc *curproc = myproc();
  
  acquire(&ptable.lock);
  for(;;){
    // Scan through table looking for exited process with given pid.
    foundpid = 0;
    for(p = ptable.proc; p < &ptable.proc[NPROC]; p++){
      if(p->pid != pid)
        continue;
      foundpid = 1;
      if(p->state == ZOMBIE){
        // Found it.

        // First off, put into the pointers the rellevant information
        *wtime = p->etime - p->ctime - p->iotime - p->rtime;
        *rtime = p->rtime;
        *iotime = p->iotime;

        // Then, do regular process ending
        kfree(p->kstack);
        p->kstack = 0;
        freevm(p->pgdir);
        p->pid = 0;
        p->parent = 0;
        p->name[0] = 0;
        p->killed = 0;
        p->state = UNUSED;
        release(&ptable.lock);
        return 0;
      }
    }

    // No point waiting if we didnt found the process.
    if(!foundpid || curproc->killed){
      release(&ptable.lock);
      return -1;
    }

    // Wait for children to exit.  (See wakeup1 call in proc_exit.)
    sleep(curproc, &ptable.lock);  //DOC: wait-sleep
  }
}

//SNIR3.4
int
setpriority(int priority)
{
  if(priority < 0 || priority > 3){
    return -1;
  }
  myproc()->priority = priority;
  return 0;
}